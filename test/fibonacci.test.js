import { describe, expect, test, beforeAll } from "bun:test";
import fib from "../src/fibonacci"
beforeAll(() => {
  // setup tests
});

describe("fibonacci", () => {
  test("0", () => {
    expect(fib(0)).toBe(0);
  });
  test("1", () => {
    expect(fib(1)).toBe(1);
  });
  test("2", () => {
    expect(fib(2)).toBe(1);
  });
  test("3", () => {
    expect(fib(3)).toBe(2);
  });
  test("4", () => {
    expect(fib(4)).toBe(3);
  });
  test("5", () => {
    expect(fib(5)).toBe(5);
  });
  test("10", () => {
    expect(fib(10)).toBe(55);
  });
  test("20", () => {
    expect(fib(20)).toBe(6765);
  });
});
