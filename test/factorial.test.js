import { describe, expect, test } from "bun:test";
import subject from "../src/factorial"


describe("factorial", () => {
  test("0", () => {
    expect(subject.factorial(0)).toBe(1);
  });
  test("1", () => {
    expect(subject.factorial(1)).toBe(1);
  });
  test("2", () => {
    expect(subject.factorial(2)).toBe(2);
  });
  test("5", () => {
    expect(subject.factorial(5)).toBe(120);
  });
});
