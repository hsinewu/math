import { describe, expect, test, beforeAll } from "bun:test";
// https://willh.gitbook.io/typescript-tutorial/

describe("primitives",()=>{
    test("boolean",()=>{
        let foo: boolean = false;
        let createdByNew = new Boolean(1)
    })
    test("number",()=>{
        let decLiteral: number = 6;
        let hexLiteral: number = 0xf00d;
        // ES6 中的二進位制表示法
        let binaryLiteral: number = 0b1010;
        // ES6 中的八進位制表示法
        let octalLiteral: number = 0o744;
        let notANumber: number = NaN;
        let infinityNumber: number = Infinity;
    })
    test("string",()=>{
        let myName: string = 'Tom';
        let myAge: number = 25;

        // 範本字串
        let sentence: string = `Hello, my name is ${myName}.
        I'll be ${myAge + 1} years old next month.`;
    })
    test("null undefined",()=>{
        {
        let u: undefined = undefined;
        let n: null = null;
        }
        
        {
        // 這樣不會報錯
        let num: number = undefined;
        }

        {
        // 這樣也不會報錯
        let u: undefined;
        let num: number = u;
        }

        {
        // Type 'void' is not assignable to type 'number'.
        let u: void;
        let num: number = u;
        }
    })
})