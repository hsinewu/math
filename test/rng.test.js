import { describe, expect, test, beforeAll } from "bun:test";
import fn from "../src/rng"

describe("rng.randomInteger", () => {
  test("default", () => {
    const x = fn.randomInteger()
    expect(typeof x).toBe('number')
  });
  test("min",()=>{
    const x = fn.randomInteger(10)
    expect(x).toBeGreaterThanOrEqual(10)
  })
  test("min max",()=>{
    const x = fn.randomInteger(10,200)
    expect(x).toBeGreaterThanOrEqual(10)
    expect(x).toBeLessThanOrEqual(200)
  })
  test("throw",()=>{
    expect(()=>fn.randomInteger(100,99)).toThrow('max should be greater than min')
  })
});
