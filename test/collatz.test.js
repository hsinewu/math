import { describe, expect, test } from "bun:test";
import fn from "../src/collatz"


describe("collatz", () => {
  test("6", () => {
    expect(fn(6)).toBe(8);
  });
  test("11", () => {
    expect(fn(11)).toBe(14);
  });
  test("27", () => {
    expect(fn(27)).toBe(111);
  });
});
