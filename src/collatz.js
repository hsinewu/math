const isOdd = (x)=>x%2==0
export default (n) => {
    const _ = (x) => {
        if(x==1) return 0
        if(isOdd(x)){
            return _(x/2)+1
        }
        return _(3*x+1)+1
    }
    return _(n)
  };
  