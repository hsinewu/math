export default {
  randomInteger(min = 0, max = 100) {
    if (max < min) throw "max should be greater than min";
    const width = max - min;

    return Math.random() * width + min;
  },
};
