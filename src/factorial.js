export default {
  factorial: (n) => {
    if (n < 0) {
      throw "error";
    }
    let _ = (x) => {
      if (x == 0) return 1;
      return x * _(x - 1);
    };
    return _(n);
  },
};
