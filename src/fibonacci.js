export default (n) => {
  let memo = [0, 1];
  let _ = (x) => {
    // if(x<=0)return 0
    // return _(x-1)+_(x-2)
    if (x in memo) return memo[x];
    return (memo[x] = _(x - 1) + _(x - 2));
  };
  return _(n);
};
